
public class Aufgaben {

	public static void main(String[] args) {
		
		//Aufgabe 1
		System.out.println("Aufgabe 1\n");
		
		String a = "*";
		System.out.printf("%4s", a);
		System.out.printf("%-4s\n", a);
		
		System.out.printf("%-4s", a);
		System.out.printf("%4s\n", a);
		
		System.out.printf("%-4s", a);
		System.out.printf("%4s\n", a);
		
		System.out.printf("%4s", a);
		System.out.printf("%-4s\n\n", a);
		
		//Aufgabe 2
		System.out.println("Aufgabe 2\n");
		
		System.out.printf("%-5s", "0!");
		System.out.printf("=");
		System.out.printf("%-19s", "");
		System.out.printf("=");
		System.out.printf("%4s\n", "1");
				
		System.out.printf("%-5s", "1!");
		System.out.printf("=");
		System.out.printf("%-19s", " 1");
		System.out.printf("=");
		System.out.printf("%4s\n", "1");
		
		System.out.printf("%-5s", "2!");
		System.out.printf("=");
		System.out.printf("%-19s", " 1 * 2");
		System.out.printf("=");
		System.out.printf("%4s", "2");
	}

}

import java.util.Scanner;

public class Treppe {

	public static void main(String[] args) {

		Scanner Tastatur = new Scanner(System.in);

		System.out.print("Wie hoch soll die Treppe sein? ");
		int hohe = Tastatur.nextInt();

		int breite = 0;

		for (int a = 0; a < hohe; a++) {

			breite++;
			for (int b = hohe - 2; b >= a; b--) {
				System.out.print(" ");
			}

			for (int c = 0; c <= breite - 1; c++) {
				System.out.print("*");
			}

			System.out.print("\n");
		}
	}
}
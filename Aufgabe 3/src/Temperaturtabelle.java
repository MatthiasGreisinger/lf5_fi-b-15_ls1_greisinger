
public class Temperaturtabelle {

	public static void main(String[] args) {
		
		//Zeile 1
		System.out.printf("%-11s", "Fahrenheit"); //Wort Fahrenheit ausgeben und auf 11 Stelle begrenzt/ freigehalten
		System.out.printf("|"); // Den Mittelstrich f�r die Tabellenabgrenzung ausgeben lassen (12. Stelle, da es in der Aufgabe so aussieht als wenn der Strich noch in den Bereich der 12 Stellen fallen soll)
		System.out.printf("%10s", "Celsius\n"); //Wort Celsius ausgeben und auf 10 Stelle begrenzt/ freigehalten
		
		//Zeile 2
		System.out.printf("%22s\n", "----------------------"); //Den Trennstrich f�r die Optik der Tabelle zwischen W�rter und Zahlen
		
		//Zeile 3
		double a = -28.8889; //Deklaration und Initialisierung
		System.out.printf("%-11s", "-20"); //Die linke Zahl f�r Fahrenheit ausgegeben
		System.out.printf("|"); // Den Mittelstrich f�r die Tabellenabgrenzung ausgeben lassen
		System.out.printf("%9.2f\n", a); //Den double Wert aus "a" auf 2 Stellen begrenzt und ausgegeben
		
		//Zeile 4
		double b = -23.3333; //Deklaration und Initialisierung
		System.out.printf("%-11s", "-10"); //Die linke Zahl f�r Fahrenheit ausgegeben
		System.out.printf("|"); // Den Mittelstrich f�r die Tabellenabgrenzung ausgeben lassen
		System.out.printf("%9.2f\n", b); //Den double Wert aus "b" auf 2 Stellen begrenzt und ausgegeben
		
		//Zeile 5
		double c = -17.7778; //Deklaration und Initialisierung
		System.out.printf("%-11s", "+0"); //Die linke Zahl f�r Fahrenheit ausgegeben
		System.out.printf("|"); // Den Mittelstrich f�r die Tabellenabgrenzung ausgeben lassen
		System.out.printf("%9.2f\n", c); //Den double Wert aus "c" auf 2 Stellen begrenzt und ausgegeben
		
		//Zeile 6
		double d = -6.6667; //Deklaration und Initialisierung
		System.out.printf("%-11s", "+20"); //Die linke Zahl f�r Fahrenheit ausgegeben
		System.out.printf("|"); // Den Mittelstrich f�r die Tabellenabgrenzung ausgeben lassen
		System.out.printf("%9.2f\n", d); //Den double Wert aus "d" auf 2 Stellen begrenzt und ausgegeben
		
		//Zeile 7
		double e = -1.1111; //Deklaration und Initialisierung
		System.out.printf("%-11s", "+30"); //Die linke Zahl f�r Fahrenheit ausgegeben
		System.out.printf("|"); // Den Mittelstrich f�r die Tabellenabgrenzung ausgeben lassen
		System.out.printf("%9.2f", e); //Den double Wert aus "e" auf 2 Stellen begrenzt und ausgegeben
		
	}

}

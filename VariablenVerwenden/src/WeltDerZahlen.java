/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Matthias Greisinger >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
	   byte anzahlPlaneten = 8;
    
    // Anzahl der Sterne in unserer Milchstra�e
       long anzahlSterne = 800000000000l;
    
    // Wie viele Einwohner hat Berlin?
       int bewohnerBerlin = 3600000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
       int alterTage = 24*365;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
       int gewichtKilogramm = 190000; //Blauwal mit 190t  
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
       int flaecheGroessteLand = 17098242; //Russland
    
    // Wie gro� ist das kleinste Land der Erde?
       short flaecheKleinsteLand = 440; //m�, Vatikanstadt
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    
    System.out.println("Einwohner in Berlin: " + bewohnerBerlin);
    
    System.out.println("Mein Alter in Tagen: " + alterTage);
    
    System.out.println("Schwerstes Tier der Welt in Kg: " + gewichtKilogramm);
    
    System.out.println("Gr��te Land der Welt in Km�: " + flaecheGroessteLand);
    
    System.out.println("Kleinste Land der Welt in m�: " + flaecheKleinsteLand);
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

